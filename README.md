# Volume bindings when running the docker container 
Run the docker container hosted in this project's gitlab container registry by
binding your local host's .ssh directory such that the identity file is 
available at /home inside the container as the download script assumes this 
location of the identity file  

`docker run -it -v /home/<username>/.ssh/:/home registry.gitlab.com/ska-telescope/sdc/sdc2-sim-environment /bin/bash`

# User workflow

1. Run docker container  
`docker run -it -v /home/<username>/.ssh/:/home registry.gitlab.com/ska-telescope/sdc/sdc2-sim-environment /bin/bash`
2. Run DownloadPrerequisites.sh script  
`bash /root/DownloadPrerequisites.sh`
3. Navigate to SDC2 directory and run the pipeline  
`cd SDC2`  
`python run_pipeline.py inis/ini_HI_server.ini`
