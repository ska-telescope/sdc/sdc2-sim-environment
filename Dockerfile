FROM ubuntu:18.04

# Install necessary system level packages
RUN apt-get update \
 && apt-get install -yq --no-install-recommends \
    wget \
    bzip2 \
    ca-certificates \
    sudo \
    locales \
    fonts-liberation \
    run-one \
    vim \
    make \
    gcc g++ \
    libbz2-dev zip \
    rsync \
    openssh-client \
 && apt-get clean && rm -rf /var/lib/apt/lists/*

# Configure python + conda environment
ENV CONDA_DIR=/opt/conda \
    SHELL=/bin/bash
ENV PATH=$CONDA_DIR/bin:$PATH 

ENV MINICONDA_VERSION=4.8.2 \
    MINICONDA_MD5=87e77f097f6ebb5127c77662dfc3165e \
    CONDA_VERSION=4.8.2

ARG PYTHON_VERSION=3.6.10

# Set up locale
RUN echo "C.UTF-8 UTF-8" > /etc/locale.gen && \
    locale-gen
ENV LC_ALL=C.UTF-8 \
    LANG=C.UTF-8

# Install and setup conda in /tmp
WORKDIR /tmp
RUN wget --quiet https://repo.continuum.io/miniconda/Miniconda3-py37_${MINICONDA_VERSION}-Linux-x86_64.sh && \
    echo "${MINICONDA_MD5} *Miniconda3-py37_${MINICONDA_VERSION}-Linux-x86_64.sh" | md5sum -c - && \
    /bin/bash Miniconda3-py37_${MINICONDA_VERSION}-Linux-x86_64.sh -f -b -p $CONDA_DIR && \
    rm Miniconda3-py37_${MINICONDA_VERSION}-Linux-x86_64.sh && \
    conda config --system --prepend channels conda-forge && \
    conda config --system --set auto_update_conda false && \
    conda config --system --set show_channel_urls true && \
    conda config --system --set channel_priority strict && \
    if [ ! $PYTHON_VERSION = 'default' ]; then conda install --yes python=$PYTHON_VERSION; fi && \
    conda install --yes conda && \
    conda install --yes pip && \
    conda update --all --yes && \
    conda clean --all -f -y

# Install the packages required for the SDC2 sim pipeline (taken from SDC2.yml)
RUN conda install --yes \
    numpy=1.18.5 \
    galsim=2.2.4\
    scipy=1.4.1\
    matplotlib=3.2.1 \
    scikit-image=0.17.2 \
    scikit-learn=0.23.1 \
    kapteyn=2.3 \
    memory_profiler=0.57.0 \
    cfitsio=3.470 && \
    conda clean --all -f -y

# Install the custom fitsio package from source copied into the image
ENV FITSDIR=/root/fitsio
RUN mkdir $FITSDIR
COPY fitsio $FITSDIR
WORKDIR $FITSDIR
RUN python setup.py install

# Copy in the download script
COPY DownloadPrerequisites.sh /root
WORKDIR /home/
